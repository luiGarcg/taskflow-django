import uuid

from django.shortcuts import render, redirect
from django.views import View
from django.urls import reverse

from user.forms import SignUpForms, LoginForms


class VerifyUserView(View):

    def get(self, request):
        create_form = LoginForms(
            initial={
                "user_id": str(uuid.uuid4())
            }
        )

        return render(
            request,
            "login.html",
            {
                "extraContext":{
                    "title": "Login"
                },
                "context":{
                    "situation": request.GET.get("situation"),
                    "form": create_form

                }
            }
        )


    def post(self, request):
        create_form = LoginForms(
            request.POST,
        )

        if create_form.is_valid():
            create_form.save()

            return redirect(
                reverse('home')
            )

        return render(
            request,
            "login.html",
            {
                "extraContext": {
                    "title": "LogIn"
                },
                "context": {
                    "situation": request.GET.get("situation"),
                    "form": create_form
                }
            }
        )


class createUserView(View):

    def get(self, request):
        create_form = SignUpForms(
            initial={
                "user_id":str(uuid.uuid4())
            }
        )
        return render(
            request,
            "signUp.html",
            {
                "extraContext":{
                    "title": "signUp"
                },
                "context":{
                    "situation": request.GET.get("situation"),
                    "form": create_form

                }
            }
        )

    def post(self, request):
        create_form = SignUpForms(
            request.POST,
        )

        if create_form.is_valid():
            create_form.save()
            return redirect(
                reverse('home')
            )

        return render(
            request,
            "Login.html",
            {
                "extraContext":{
                    "title": "SignUp"
                },
                "context":{
                    "situation": request.GET.get("situation"),
                    "form": create_form
                }
            }
        )
