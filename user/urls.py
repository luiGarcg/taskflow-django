from django.urls import path

from user.views import VerifyUserView, createUserView

urlpatterns = [
    path('logIn/', VerifyUserView.as_view(), name='logIn'),
    path('signUp/', createUserView.as_view(), name='signUp'),
]
