import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models

class User(AbstractUser):

    user_id = models.UUIDField(
        primary_key=True,
        auto_created=True,
        default=str(uuid.uuid4())
    )
    user_name = models.CharField(max_length=100)
    user_email = models.CharField(max_length=100)
    user_password = models.CharField(max_length=100)

    def to_dict(self):
        return{

            "user_id": str(self.user_id),
            "user_name": self.user_name,
            "user_email": self.user_email,
            "user_password": self.user_password,
        }

    def __str__(self):
       return f'User: {self.user_id}'


