from django import forms

from user.models import User


class SignUpForms(forms.ModelForm):

    user_id = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'type':"text",
                'class': "form-control mb-3 w-75",
                'name': "name",
                'placeholder': "name",
                'id':"name",
                'readonly': True
            }
        )
    )

    user_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'type':"text",
                'class': "form-control mb-3 w-75",
                'name': "name",
                'placeholder': "name",
                'id':"name"
            }
        )
    )

    user_email = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'type':"text",
                'class': "form-control mb-3 w-75",
                'name': "email",
                'placeholder': "email",
                'id':"email "
            }
        )
    )

    user_password = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'type':"password",
                'class': "form-control mb-3 w-75",
                'name': "password",
                'placeholder': "password",
                'id':"password"
            }
        )
    )

    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, detail: bool = False, *args, **kwargs):
        super(SignUpForms, self).__init__(*args, **kwargs)

class LoginForms(forms.ModelForm):

    user_id = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'type': "text",
                'class': "form-control mb-3 w-75",
                'name': "email",
                'id': "email ",
                'placeholder': "email",
                'readonly': True
            }
        )
    )

    user_email = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'type': "text",
                'class': "form-control mb-3 w-75",
                'name': "email",
                'placeholder': "email",
                'id': "email "
            }
        )
    )

    user_password = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'type': "password",
                'class': "form-control mb-3 w-75",
                'name': "password",
                'placeholder': "password",
                'id': "password"
            }
        )
    )

    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, detail: bool = False, *args, **kwargs):
        super(LoginForms, self).__init__(*args, **kwargs)

        if detail:
            for _, field in self.fields.items():
                field.widget.attr