from django.db import models

# Create your models here.
from django.db import models
import uuid

# Create your models here.
class Task(models.Model):

    tas_id = models.UUIDField(
        primary_key=True,
        auto_created=True,
        default=str(uuid.uuid4())
    )
    tas_title = models.CharField(max_length=100)
    tas_description = models.TextField(max_length=1024)
    tas_status = models.CharField(max_length=100)
    tas_team = models.CharField(max_length=100)
    tas_startDate = models.CharField(max_length=10)
    tas_finalDate = models.CharField(max_length=10)


    def to_dict(self):
        return{

            "tas_id": str(self.tas_id),
            "tas_title": self.tas_title,
            "tas_description": self.tas_description,
            "tas_status": self.tas_status,
            "tas_team": self.tas_team,
            "tas_startDate": self.tas_startDate,
            "tas_finalDate": self.tas_finalDate,
        }

    def __str__(self):
       return f'task: {self.tas_id}'


